package com.company;

public class User {

    private int id;
    private static int id_gen = 0;
    private String name;
    private String surname;
    private String username;
    private Password password;

    public User(String name , String surname , String username , String password) {
        this();
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
    }

    public User(String s, String s1) {
        this();
    }

    public User() {
        generatedId();
    }

    public User(int id, String name, String surname, String username, String password) {
        this.id = id;
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
    }

    private void generatedId() {
        id = id_gen++;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public String getSurname() {
        return surname;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getUsername() {
        return username;
    }
    public void setPassword(String password) {
        this.password = new Password(password);
    }
    public String getPassword() {
        return password.getPass();
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + surname + " " + username + " " + password;
    }
}